using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIDisplay : MonoBehaviour
{
    [Header("Score")]
    ScoreKeeper score;
    [SerializeField] TextMeshProUGUI scoreText;


    [Header("Health")]
    [SerializeField] Hitpoints playerHitpoints;
    [SerializeField] Slider hitpointsSlider;



    // Update is called once per frame
    void Awake()
    {
        score = FindObjectOfType<ScoreKeeper>();

    }

    void Start()
    {
        hitpointsSlider.maxValue = playerHitpoints.getHitpoints();
    }

    void Update()
    {
        hitpointsSlider.value = playerHitpoints.getHitpoints();
        scoreText.text = score.getCurentScore().ToString("00000000000");
    }
}
