using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFinder : MonoBehaviour
{
    EnemySpawner EnemySpawner;
    WaveConfig waveConfig;
    List<Transform> wavepoints;
    int wayPointIndex = 0;

    void Awake()
    {
        EnemySpawner = FindObjectOfType<EnemySpawner>();
    }
    void Start()
    {
        waveConfig = EnemySpawner.GetCurrentWave();

        wavepoints = waveConfig.GetWaypoints();
        transform.position = wavepoints[wayPointIndex].position;


    }


    void Update()
    {
        FollowPath();

    }

    private void FollowPath()
    {
        if (wayPointIndex < wavepoints.Count)
        {
            Vector3 targetPosition = wavepoints[wayPointIndex].position;
            float delta = waveConfig.GetMoveSpeed() * Time.deltaTime;
            transform.position = Vector2.MoveTowards(transform.position, targetPosition, delta);
            if (transform.position == targetPosition)
            {
                wayPointIndex += 1;
            }

        }
        else
        {
            Destroy(gameObject);
        }
    }
}
