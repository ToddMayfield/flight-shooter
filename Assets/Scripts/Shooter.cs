using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{

    [Header("General Variables")]

    [SerializeField] GameObject projectilePrefab;
    [SerializeField] float projectileSpeed = 10f;
    [SerializeField] float projectileLifeTime = 5f;
    [SerializeField] float baseFiringRate = 0.2f;

    [Header("AI Variables")]
    [SerializeField] bool useAI;
    [SerializeField] float firingRateVariance = 0f;
    [SerializeField] float minFiringRate = 0.1f; //Stops negative firing times.
    [Header("Audio")]
    AudioPlayer audioPlayer;



    [HideInInspector] public bool isFiring;

    Coroutine firingCoroutine;


    void Awake()
    {
        audioPlayer = FindObjectOfType<AudioPlayer>();
    }

    void Start()
    {
        if (useAI)
        {
            isFiring = true;
        }

    }

    void Update()
    {
        Fire();

    }

    private void Fire()
    {
        if (isFiring && firingCoroutine == null)
        {
            firingCoroutine = StartCoroutine(FireContinously());
        }
        else if (!isFiring && firingCoroutine != null)
        {
            StopCoroutine(firingCoroutine);
            firingCoroutine = null;
        }


    }

    IEnumerator FireContinously()
    {
        while (true)
        {

            // audioPlayer.PlayShootingClip();
            GameObject projectileInstance = Instantiate(projectilePrefab,
            transform.position,
            Quaternion.identity);

            Rigidbody2D rb = projectileInstance.GetComponent<Rigidbody2D>();

            if (rb != null)
            {
                rb.velocity = (-transform.up) * projectileSpeed;
            }

            Destroy(projectileInstance, projectileLifeTime);

            float TimeToNextProjectile = Random.Range(baseFiringRate - firingRateVariance,
            baseFiringRate + firingRateVariance);
            TimeToNextProjectile = Mathf.Clamp(TimeToNextProjectile, minFiringRate, 2f);

            audioPlayer.PlayShootingClip();


            if (useAI)
            {
                yield return new WaitForSeconds(GetRandomFiringTime());
            }
            else
            {
                yield return new WaitForSeconds(baseFiringRate);
            }
        }

    }

    public float GetRandomFiringTime()
    {
        float spawnTime = Random.Range(baseFiringRate - firingRateVariance,
        baseFiringRate + firingRateVariance);
        return Mathf.Clamp(spawnTime, minFiringRate, float.MaxValue);
    }
}
